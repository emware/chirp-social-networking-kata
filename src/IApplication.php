<?php

namespace Chirp;

/**
 * Interface IApplication.
 */
interface IApplication
{
    /**
     * Start the application runtime.
     */
    public function start(): void;

    /**
     * Force application termination.
     */
    public function terminate(): void;

    /**
     * Return the internal application running status.
     *
     * @return bool
     */
    public function isRunning(): bool;
}
