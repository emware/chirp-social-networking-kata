<?php

namespace Chirp\Storage\Interfaces;

use Closure;
use Chirp\Entity\Interfaces\IPersistentEntity;

/**
 * Interface IPersistenceManager.
 */
interface IPersistenceManager
{
    /**
     * @param IPersistentEntity $entity
     *
     * @return bool
     */
    public function store(IPersistentEntity $entity): bool;

    /**
     * @param string $className
     *
     * @return IPersistenceManager
     */
    public function select($className): IPersistenceManager;

    /**
     * @param Closure $filter
     *
     * @return IPersistenceManager
     */
    public function filter(Closure $filter): IPersistenceManager;

    /**
     * @return array
     */
    public function get(): array;

    /**
     * @return mixed
     */
    public function flush(): void;
}
