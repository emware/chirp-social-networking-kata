<?php

namespace Chirp\Storage;

use Closure;
use Chirp\Entity\Interfaces\IPersistentEntity;
use Chirp\Storage\Interfaces\IPersistenceManager;

/**
 * Class PersistenceManager.
 */
class PersistenceManager implements IPersistenceManager
{
    /**
     * @var array
     */
    private $persistentStorage = [];

    /**
     * @var array
     */
    private $searchPool = [];

    /**
     * Retrieve the persistent storage for a specific entity type.
     *
     * @param string $fqn
     *
     * @return array
     */
    private function getPersistentEntity($fqn)
    {
        if (!isset($this->persistentStorage[$fqn])) {
            $this->persistentStorage[$fqn] = [];
        }

        return $this->persistentStorage[$fqn];
    }

    /**
     * Add an Entity to the persistent storage.
     *
     * @param IPersistentEntity $entity
     */
    private function persistEntity(IPersistentEntity $entity)
    {
        array_unshift($this->persistentStorage[get_class($entity)], $entity);
    }

    /**
     * Reset current search pool.
     */
    private function resetSearchPool()
    {
        $this->searchPool = [];
    }

    /**
     * @param IPersistentEntity $entity
     *
     * @return bool
     */
    public function store(IPersistentEntity $entity): bool
    {
        if (in_array($entity, $this->getPersistentEntity(get_class($entity)))) {
            return false;
        }

        $this->persistEntity($entity);

        return true;
    }

    /**
     * Let client define the domain namespace over where perform the search.
     *
     * @param string $fqn
     *
     * @return IPersistenceManager
     */
    public function select($fqn): IPersistenceManager
    {
        if (is_subclass_of($fqn, IPersistentEntity::class, true)) {
            $this->searchPool = $this->getPersistentEntity($fqn);
        }

        return $this;
    }

    /**
     * Apply a closure to the current search pool.
     *
     * @param Closure $filter
     *
     * @return IPersistenceManager
     */
    public function filter(Closure $filter): IPersistenceManager
    {
        foreach ($this->searchPool as $key => $entity) {
            if (!$filter($entity)) {
                unset($this->searchPool[$key]);
            }
        }

        return $this;
    }

    /**
     * Return the search result.
     *
     * @return array
     */
    public function get(): array
    {
        $currentPool = $this->searchPool;

        $this->resetSearchPool();

        return array_values($currentPool);
    }

    /**
     * Flush all persisted entities.
     */
    public function flush(): void
    {
        $this->resetSearchPool();
        $this->persistentStorage = [];
    }
}
