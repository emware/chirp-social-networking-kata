<?php

namespace Chirp\Entity;

use Chirp\Entity\Interfaces\IPersistentEntity;

/**
 * Class User.
 */
class User implements IPersistentEntity
{
    /**
     * User's username.
     *
     * @var string
     */
    private $username;

    /**
     * List of followed users.
     *
     * @var User[]
     */
    private $followedUsers = [];

    /**
     * User constructor.
     *
     * @param $username
     */
    public function __construct($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function follow(User $user)
    {
        $this->followedUsers[$user->getUsername()] = $user;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getFollowedUsers()
    {
        return array_values($this->followedUsers);
    }
}
