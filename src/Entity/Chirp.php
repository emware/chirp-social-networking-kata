<?php

namespace Chirp\Entity;

use Carbon\Carbon;
use Chirp\Entity\Interfaces\IPersistentEntity;

/**
 * Class Chirp.
 */
class Chirp implements IPersistentEntity
{
    /**
     * Chirp's creator.
     *
     * @var User
     */
    private $creator;

    /**
     * Chirp's message.
     *
     * @var string
     */
    private $message;

    /**
     * Chirp's creation date.
     *
     * @var Carbon
     */
    private $createdAt;

    /**
     * Chirp constructor.
     *
     * @param User $user
     * @param $message
     */
    public function __construct(User $user, $message)
    {
        $this->creator = $user;
        $this->message = $message;
        $this->createdAt = new Carbon();
    }

    /**
     * @return User
     */
    public function createdBy(): User
    {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * @return Carbon
     */
    public function createdAt(): Carbon
    {
        return $this->createdAt;
    }
}
