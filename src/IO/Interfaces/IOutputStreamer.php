<?php

namespace Chirp\IO\Interfaces;

/**
 * Interface IOutputStreamer.
 */
interface IOutputStreamer
{
    /**
     * Put a single stream of output data.
     *
     * @param string $output
     */
    public function setOutput(string $output): void;
}
