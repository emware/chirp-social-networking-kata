<?php

namespace Chirp\IO\Interfaces;

/**
 * Interface IInputStreamer.
 */
interface IInputStreamer
{
    /**
     * Return a single block of input data.
     *
     * @return string
     */
    public function getInput(): string;
}
