<?php

namespace Chirp\IO;

use Chirp\IO\Interfaces\IInputStreamer;
use Chirp\IO\Interfaces\IOutputStreamer;

/**
 * Class Console
 * Implements Input/Output handling for shell application.
 */
class Console implements IInputStreamer, IOutputStreamer
{
    /**
     * Shell newline caret displayed at each line of input/output.
     */
    const SHELL_NEWLINE_PLACEHOLDER = '> ';

    /**
     * @var resource
     */
    private $inputStream;

    /**
     * @var resource
     */
    private $outputStream;

    /**
     * Console constructor.
     *
     * @param $inputStream
     * @param $outputStream
     */
    public function __construct($inputStream, $outputStream)
    {
        $this->inputStream = $inputStream;
        $this->outputStream = $outputStream;
    }

    /**
     * Read single line of input from STDIN.
     *
     * @return string
     */
    public function getInput(): string
    {
        echo self::SHELL_NEWLINE_PLACEHOLDER;

        return stream_get_line($this->inputStream, 1024, PHP_EOL);
    }

    /**
     * Use echo function for output required data.
     *
     * @param string $output
     */
    public function setOutput(string $output): void
    {
        if (!$output) {
            return;
        }

        fwrite($this->outputStream, sprintf('%s%s%s', self::SHELL_NEWLINE_PLACEHOLDER, trim($output), PHP_EOL));
    }
}
