<?php

namespace Chirp\CommandFactory\Interfaces;

use Chirp\CommandFactory\DTO\InputDTO;

/**
 * Interface IStatementInterpreter.
 */
interface IStatementInterpreter
{
    /**
     * @param string $statement
     *
     * @return InputDTO
     */
    public function interpret(string $statement): InputDTO;
}
