<?php

namespace Chirp\CommandFactory\Interfaces;

use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Interface IGenericCommand.
 */
interface IGenericCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO;
}
