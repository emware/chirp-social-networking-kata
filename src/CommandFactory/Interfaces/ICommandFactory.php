<?php

namespace Chirp\CommandFactory\Interfaces;

use Chirp\IApplication;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\Storage\Interfaces\IPersistenceManager;

/**
 * Interface ICommandFactory.
 */
interface ICommandFactory
{
    /**
     * ICommandFactory constructor.
     *
     * @param IApplication        $application
     * @param IPersistenceManager $persistenceManager
     */
    public function __construct(IApplication $application, IPersistenceManager $persistenceManager);

    /**
     * @param InputDTO $inputDTO
     *
     * @return IGenericCommand
     */
    public function makeCommand(InputDTO $inputDTO): IGenericCommand;
}
