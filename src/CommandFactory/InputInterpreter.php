<?php

namespace Chirp\CommandFactory;

use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Interfaces\IStatementInterpreter;

/**
 * Class InputInterpreter.
 */
class InputInterpreter implements IStatementInterpreter
{
    /**
     * Operator element identification.
     */
    const OPERATOR_STATEMENT_ELEMENT = 'operator';

    /**
     * Parameter element identification.
     */
    const PARAMETER_STATEMENT_ELEMENT = 'parameter';

    /**
     * Username element identification.
     */
    const USERNAME_STATEMENT_ELEMENT = 'username';

    /**
     * Command parameters detection regular expression.
     */
    const COMMAND_MATCH_REGEX = "/^((?!%s)\w+){0,1}\s*(?(?<=\s)(%s)|(%s)){0,1}\s*(.+){0,1}/";

    /**
     * The list of operators that will be matched while interpreting the input.
     *
     * @var array
     */
    private $operators = [];

    /**
     * The list of system command operators.
     *
     * @var array
     */
    private $systemOperators;

    /**
     * Current instance regular expression.
     *
     * @var string
     */
    private $builtRegex;

    /**
     * InputInterpreter constructor.
     *
     * @param array $operators
     * @param array $systemOperators
     */
    public function __construct(array $operators, array $systemOperators = [])
    {
        $this->operators = $operators;
        $this->systemOperators = $systemOperators;

        $this->builtRegex = $this->buildRegexForRequiredOperators();
    }

    /**
     * Build the regular expression for detection of current required set of command operators.
     *
     * @return string
     */
    private function buildRegexForRequiredOperators(): string
    {
        return sprintf(self::COMMAND_MATCH_REGEX, implode('|', $this->systemOperators), implode('|', $this->operators), implode('|', $this->systemOperators));
    }

    /**
     * @param string $statement
     *
     * @return array
     */
    private function getCommandArguments(string $statement): array
    {
        preg_match($this->builtRegex, $statement, $matches, PREG_OFFSET_CAPTURE);

        $matches = array_column($matches, 0);

        $arguments[self::USERNAME_STATEMENT_ELEMENT] = $matches[1] ?? '';
        $arguments[self::OPERATOR_STATEMENT_ELEMENT] = !empty($matches[2]) ? $matches[2] : (!empty($matches[3]) ? $matches[3] : '');
        $arguments[self::PARAMETER_STATEMENT_ELEMENT] = $matches[4] ?? '';

        return $arguments;
    }

    /**
     * @param string $statement
     *
     * @return InputDTO
     */
    public function interpret(string $statement): InputDTO
    {
        $arguments = $this->getCommandArguments($statement);

        list(self::USERNAME_STATEMENT_ELEMENT => $username, self::OPERATOR_STATEMENT_ELEMENT => $operator, self::PARAMETER_STATEMENT_ELEMENT => $parameter) = $arguments;

        return (new InputDTO($operator))
            ->setUsername($username)
            ->setParameter($parameter);
    }
}
