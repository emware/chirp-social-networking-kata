<?php

namespace Chirp\CommandFactory\Command;

use Chirp\IApplication;

/**
 * Class AbstractApplicationAwareCommand.
 */
abstract class AbstractApplicationAwareCommand extends AbstractCommand
{
    /**
     * @var IApplication
     */
    private $application;

    /**
     * AbstractApplicationAwareCommand constructor.
     *
     * @param IApplication $application
     * @param $parameter
     *
     * @internal param IApplication $application≤
     */
    public function __construct(IApplication $application, $parameter = null)
    {
        parent::__construct($parameter);

        $this->application = $application;
    }

    /**
     * @return IApplication
     */
    protected function getApplication(): IApplication
    {
        return $this->application;
    }
}
