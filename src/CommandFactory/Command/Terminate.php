<?php

namespace Chirp\CommandFactory\Command;

use Chirp\CommandFactory\DTO\ResponseDTO;

class Terminate extends AbstractApplicationAwareCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO
    {
        if ($this->getApplication()->isRunning()) {
            $this->getApplication()->terminate();
        }

        return new ResponseDTO(!$this->getApplication()->isRunning(), $this->getApplication()->isRunning() ? ['Error: application could not be terminated'] : ['Application terminated']);
    }
}
