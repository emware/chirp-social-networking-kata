<?php

namespace Chirp\CommandFactory\Command;

use Chirp\Entity\Chirp;
use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Class Read.
 */
class Read extends AbstractApplicantUserAwareCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO
    {
        $applicantUser = $this->getApplicantUser();

        $chirps = $this->getPersistenceManager()
            ->select(Chirp::class)
            ->filter(function (Chirp $entity) use ($applicantUser) {
                return $entity->createdBy()->getUsername() == $applicantUser->getUsername();
            })
            ->get();

        $messages = [];
        foreach ($chirps as $chirp) {
            /* @var $chirp Chirp */
            $messages[] = sprintf('%s (%s)', $chirp->message(), $chirp->createdAt()->diffForHumans());
        }

        return new ResponseDTO(count($messages), $messages);
    }
}
