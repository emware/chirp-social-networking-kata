<?php

namespace Chirp\CommandFactory\Command;

use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Class Follow.
 */
class Follow extends AbstractApplicantUserAwareCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO
    {
        $targetUser = $this->findUserByUsername($this->getParameter());

        if ($targetUser->getUsername() == $this->getApplicantUser()->getUsername()) {
            return new ResponseDTO(false, ['You cannot follow yourself']);
        }

        foreach ($this->getApplicantUser()->getFollowedUsers() as $followedUser) {
            if ($followedUser->getUsername() == $targetUser->getUsername()) {
                return new ResponseDTO(false, [sprintf('%s is already following %s', $this->getApplicantUser()->getUsername(), $targetUser->getUsername())]);
            }
        }

        $this->getApplicantUser()->follow($targetUser);

        return new ResponseDTO(true, [sprintf('%s now is following %s', $this->getApplicantUser()->getUsername(), $targetUser->getUsername())]);
    }
}
