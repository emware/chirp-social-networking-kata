<?php

namespace Chirp\CommandFactory\Command;

use Chirp\Storage\Interfaces\IPersistenceManager;

/**
 * Class AbstractPersistenceLayerAwareCommand.
 */
abstract class AbstractPersistenceLayerAwareCommand extends AbstractCommand
{
    /**
     * @var IPersistenceManager
     */
    private $persistenceManager;

    /**
     * @param IPersistenceManager $persistenceManager
     * @param mixed               $parameter
     */
    public function __construct(IPersistenceManager $persistenceManager, $parameter = null)
    {
        parent::__construct($parameter);

        $this->persistenceManager = $persistenceManager;
    }

    /**
     * @return IPersistenceManager
     */
    protected function getPersistenceManager(): IPersistenceManager
    {
        return $this->persistenceManager;
    }
}
