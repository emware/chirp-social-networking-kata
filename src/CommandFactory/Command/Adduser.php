<?php

namespace Chirp\CommandFactory\Command;

use Chirp\Entity\User;
use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Class Adduser.
 */
class Adduser extends AbstractPersistenceLayerAwareCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO
    {
        $username = $this->getParameter();

        if (!$username) {
            return new ResponseDTO(false, ['Username should be provided in order to create a new User']);
        }

        $username = str_replace(' ', '_', $username);

        $userExists = $this->getPersistenceManager()
            ->select(User::class)
            ->filter(function (User $entity) use ($username) {
                return $entity->getUsername() == $username;
            })
            ->get();

        if (count($userExists)) {
            return new ResponseDTO(false, [sprintf('User with username "%s" already exists', $username)]);
        }

        $user = new User($username);

        $result = $this->getPersistenceManager()->store($user);

        return new ResponseDTO($result, $result ? ['User created successfully!'] : ['Internal error: could not create User']);
    }
}
