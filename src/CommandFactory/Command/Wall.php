<?php

namespace Chirp\CommandFactory\Command;

use Chirp\Entity\Chirp;
use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Class Wall.
 */
class Wall extends AbstractApplicantUserAwareCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO
    {
        $applicantUser = $this->getApplicantUser();

        $chirps = $this->getPersistenceManager()
                       ->select(Chirp::class)
                       ->filter(function (Chirp $entity) use ($applicantUser) {
                           $usernameFilters[] = $applicantUser->getUsername();

                           foreach ($applicantUser->getFollowedUsers() as $followedUser) {
                               $usernameFilters[] = $followedUser->getUsername();
                           }

                           return in_array($entity->createdBy()->getUsername(), $usernameFilters);
                       })
                       ->get();

        $messages = [];
        foreach ($chirps as $chirp) {
            /* @var $chirp Chirp */
            $messages[] = sprintf('%s - %s (%s)', $chirp->createdBy()->getUsername(), $chirp->message(), $chirp->createdAt()->diffForHumans());
        }

        return new ResponseDTO(count($messages), $messages);
    }
}
