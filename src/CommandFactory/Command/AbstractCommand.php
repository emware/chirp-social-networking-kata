<?php

namespace Chirp\CommandFactory\Command;

use Chirp\CommandFactory\DTO\ResponseDTO;
use Chirp\CommandFactory\Interfaces\IGenericCommand;

/**
 * Class AbstractCommand.
 */
abstract class AbstractCommand implements IGenericCommand
{
    /**
     * @var mixed
     */
    private $parameter;

    /**
     * AbstractCommand constructor.
     *
     * @param $parameter
     */
    public function __construct($parameter = null)
    {
        $this->parameter = $parameter;
    }

    /**
     * @return mixed
     */
    protected function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @return ResponseDTO
     */
    abstract public function execute(): ResponseDTO;
}
