<?php

namespace Chirp\CommandFactory\Command;

use Chirp\Entity\Chirp as ChirpEntity;
use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Class Chirp.
 */
class Chirp extends AbstractApplicantUserAwareCommand
{
    /**
     * @return ResponseDTO
     */
    public function execute(): ResponseDTO
    {
        $chirp = new ChirpEntity($this->getApplicantUser(), $this->getParameter());

        $result = $this->getPersistenceManager()->store($chirp);

        return new ResponseDTO($result, $result ? [] : [sprintf('%s has already chirped this message!')]);
    }
}
