<?php

namespace Chirp\CommandFactory\Command;

use Chirp\Entity\User;
use Chirp\Storage\Interfaces\IPersistenceManager;

/**
 * Class AbstractApplicantUserAwareCommand.
 */
abstract class AbstractApplicantUserAwareCommand extends AbstractPersistenceLayerAwareCommand
{
    /**
     * @var User
     */
    private $applicantUser;

    /**
     * AbstractApplicantUserAwareCommand constructor.
     *
     * @param string              $username
     * @param IPersistenceManager $persistenceManager
     * @param $parameter
     */
    public function __construct(IPersistenceManager $persistenceManager, string $username, $parameter = null)
    {
        parent::__construct($persistenceManager, $parameter);

        $this->applicantUser = $this->findUserByUsername($username);
    }

    /**
     * @return User
     */
    protected function getApplicantUser(): User
    {
        return $this->applicantUser;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    protected function findUserByUsername(string $username): User
    {
        $users = $this->getPersistenceManager()
            ->select(User::class)
            ->filter(function (User $entity) use ($username) {
                return $entity->getUsername() == $username;
            })
            ->get();

        return array_shift($users);
    }
}
