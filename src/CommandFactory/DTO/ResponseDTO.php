<?php

namespace Chirp\CommandFactory\DTO;

/**
 * Class CommandResponse.
 */
class ResponseDTO
{
    /**
     * @var bool
     */
    private $success;

    /**
     * @var array
     */
    private $messages;

    /**
     * CommandResponse constructor.
     *
     * @param $success
     * @param array $messages
     */
    public function __construct($success, array $messages = [])
    {
        $this->messages = $messages;
        $this->success = $success;
    }

    /**
     * @return bool
     */
    public function success(): bool
    {
        return (bool) $this->success;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return $this->messages;
    }
}
