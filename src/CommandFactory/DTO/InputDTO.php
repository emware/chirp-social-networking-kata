<?php

namespace Chirp\CommandFactory\DTO;

/**
 * Class InputDTO.
 */
class InputDTO
{
    /**
     * @var string
     */
    private $operator;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $parameter = '';

    /**
     * InputDTO constructor.
     *
     * @param string $operator
     */
    public function __construct(string $operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return InputDTO
     */
    public function setUsername(string $username): InputDTO
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getParameter(): string
    {
        return $this->parameter;
    }

    /**
     * @param string $parameter
     *
     * @return InputDTO
     */
    public function setParameter(string $parameter): InputDTO
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }
}
