<?php

namespace Chirp\CommandFactory;

use Chirp\IApplication;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\Storage\Interfaces\IPersistenceManager;
use Chirp\CommandFactory\Interfaces\IGenericCommand;
use Chirp\CommandFactory\Factory\AbstractCommandFactory;
use Chirp\CommandFactory\Interfaces\IStatementInterpreter;
use Chirp\CommandFactory\Factory\ApplicationAwareCommandFactory;
use Chirp\CommandFactory\Factory\ApplicantUserAwareCommandFactory;
use Chirp\CommandFactory\Factory\PersistenceLayerAwareCommandFactory;

/**
 * Class CommandDetector.
 */
class CommandDetector
{
    /**
     * List of currently supported commands.
     */
    const SUPPORTED_COMMANDS = [
        '' => ApplicantUserAwareCommandFactory::class,
        'follows' => ApplicantUserAwareCommandFactory::class,
        '->' => ApplicantUserAwareCommandFactory::class,
        'wall' => ApplicantUserAwareCommandFactory::class,
    ];

    /**
     * List of system commands.
     */
    const SYSTEM_COMMANDS = [
        'exit' => ApplicationAwareCommandFactory::class,
        'adduser' => PersistenceLayerAwareCommandFactory::class,
    ];

    /**
     * @var IPersistenceManager
     */
    private $persistenceManager;

    /**
     * @var IApplication
     */
    private $application;

    /**
     * @var IStatementInterpreter
     */
    private $interpreter;

    /**
     * @var array
     */
    private $factories = [];

    /**
     * CommandDetector constructor.
     *
     * @param IApplication        $application
     * @param IPersistenceManager $persistenceManager
     */
    public function __construct(IApplication $application, IPersistenceManager $persistenceManager)
    {
        $this->application = $application;
        $this->persistenceManager = $persistenceManager;

        $this->interpreter = new InputInterpreter(array_filter(array_keys(self::SUPPORTED_COMMANDS)), array_keys(self::SYSTEM_COMMANDS));
    }

    /**
     * @param string $statement
     *
     * @return InputDTO
     */
    private function interpretInputStatement(string $statement): InputDTO
    {
        return $this->interpreter->interpret($statement);
    }

    /**
     * @param string $operator
     *
     * @return AbstractCommandFactory
     */
    private function getFactory(string $operator): AbstractCommandFactory
    {
        $operators = self::SUPPORTED_COMMANDS + self::SYSTEM_COMMANDS;

        $factoryClass = $operators[$operator] ?? '';

        if (!isset($this->factories[$factoryClass])) {
            $this->factories[$factoryClass] = new $factoryClass($this->application, $this->persistenceManager);
        }

        return $this->factories[$factoryClass];
    }

    /**
     * @param string $statement
     *
     * @return IGenericCommand
     *
     * @throws \Exception
     */
    public function makeCommand(string $statement): IGenericCommand
    {
        $inputDTO = $this->interpretInputStatement($statement);

        if (!$inputDTO->getOperator() and !$inputDTO->getUsername()) {
            throw new \Exception('Command not detected: invalid input');
        }

        return $this->getFactory($inputDTO->getOperator())->makeCommand($inputDTO);
    }
}
