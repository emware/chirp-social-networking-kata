<?php

namespace Chirp\CommandFactory\Factory;

use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Command\Adduser;
use Chirp\CommandFactory\Interfaces\IGenericCommand;

/**
 * Class PersistenceLayerAwareCommandFactory.
 */
class PersistenceLayerAwareCommandFactory extends AbstractCommandFactory
{
    /**
     * This method should be overridden for define the list of factory supported commands.
     */
    protected function configureSupportedCommands(): void
    {
        $this->addCommand('adduser', Adduser::class);
    }

    /**
     * Instantiate the concrete command following the concrete factory construction methodology.
     *
     * @param string   $class
     * @param InputDTO $inputDTO
     *
     * @return IGenericCommand
     */
    protected function instantiateCommand(string $class, InputDTO $inputDTO): IGenericCommand
    {
        return new $class($this->getPersistenceManager(), $inputDTO->getParameter());
    }
}
