<?php

namespace Chirp\CommandFactory\Factory;

use Chirp\IApplication;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\Storage\Interfaces\IPersistenceManager;
use Chirp\CommandFactory\Interfaces\ICommandFactory;
use Chirp\CommandFactory\Interfaces\IGenericCommand;

/**
 * Class AbstractCommandFactory.
 */
abstract class AbstractCommandFactory implements ICommandFactory
{
    /**
     * @var IApplication
     */
    private $application;

    /**
     * @var IPersistenceManager
     */
    private $persistenceManager;

    /**
     * @var array
     */
    private $supportedCommands = [];

    /**
     * AbstractCommandFactory constructor.
     *
     * @param IApplication        $application
     * @param IPersistenceManager $persistenceManager
     */
    final public function __construct(IApplication $application, IPersistenceManager $persistenceManager)
    {
        $this->application = $application;
        $this->persistenceManager = $persistenceManager;

        $this->configureSupportedCommands();
    }

    /**
     * This method should be overridden for define the list of factory supported commands.
     */
    abstract protected function configureSupportedCommands(): void;

    /**
     * Instantiate the concrete command following the concrete factory construction methodology.
     *
     * @param string   $class
     * @param InputDTO $inputDTO
     *
     * @return IGenericCommand
     */
    abstract protected function instantiateCommand(string $class, InputDTO $inputDTO): IGenericCommand;

    /**
     * @param string $operator
     * @param string $class
     */
    final protected function addCommand(string $operator, string $class): void
    {
        $this->supportedCommands[$operator] = $class;
    }

    /**
     * @param string $operator
     *
     * @return string
     */
    final private function getCommandClassForOperator(string $operator): string
    {
        return $this->supportedCommands[$operator] ?? '';
    }

    /**
     * @return IApplication
     */
    final protected function getApplication(): IApplication
    {
        return $this->application;
    }

    /**
     * @return IPersistenceManager
     */
    final protected function getPersistenceManager(): IPersistenceManager
    {
        return $this->persistenceManager;
    }

    /**
     * @param InputDTO $inputDTO
     *
     * @return IGenericCommand
     */
    public function makeCommand(InputDTO $inputDTO): IGenericCommand
    {
        return $this->instantiateCommand($this->getCommandClassForOperator($inputDTO->getOperator()), $inputDTO);
    }
}
