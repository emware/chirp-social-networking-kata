<?php

namespace Chirp\CommandFactory\Factory;

use Chirp\CommandFactory\Command\Read;
use Chirp\CommandFactory\Command\Wall;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Command\Chirp;
use Chirp\CommandFactory\Command\Follow;
use Chirp\CommandFactory\Interfaces\IGenericCommand;

/**
 * Class ApplicantUserAwareCommandFactory.
 */
class ApplicantUserAwareCommandFactory extends AbstractCommandFactory
{
    /**
     * This method should be overridden for define the list of factory supported commands.
     */
    protected function configureSupportedCommands(): void
    {
        $this->addCommand('', Read::class);
        $this->addCommand('->', Chirp::class);
        $this->addCommand('follows', Follow::class);
        $this->addCommand('wall', Wall::class);
    }

    /**
     * Instantiate the concrete command following the concrete factory construction methodology.
     *
     * @param string   $class
     * @param InputDTO $inputDTO
     *
     * @return IGenericCommand
     */
    protected function instantiateCommand(string $class, InputDTO $inputDTO): IGenericCommand
    {
        return new $class($this->getPersistenceManager(), $inputDTO->getUsername(), $inputDTO->getParameter());
    }
}
