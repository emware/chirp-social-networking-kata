<?php

namespace Chirp\CommandFactory\Factory;

use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Command\Terminate;
use Chirp\CommandFactory\Interfaces\IGenericCommand;

/**
 * Class ApplicationAwareCommandFactory.
 */
class ApplicationAwareCommandFactory extends AbstractCommandFactory
{
    /**
     * This method should be overridden for define the list of factory supported commands.
     */
    protected function configureSupportedCommands(): void
    {
        $this->addCommand('exit', Terminate::class);
    }

    /**
     * Instantiate the concrete command following the concrete factory construction methodology.
     *
     * @param string   $class
     * @param InputDTO $inputDTO
     *
     * @return IGenericCommand
     */
    protected function instantiateCommand(string $class, InputDTO $inputDTO): IGenericCommand
    {
        return new $class($this->getApplication(), $inputDTO->getParameter());
    }
}
