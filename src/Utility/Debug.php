<?php

/**
 * @param mixed $variable
 */
function r($variable)
{
    $backtrace = debug_backtrace();
    $exitPoint = sprintf('exitpoint: %s@%s line %s', $backtrace[1]['class'], $backtrace[1]['function'], $backtrace[0]['line']);

    dump($variable);
    dump($exitPoint);

    exit();
}
