<?php

namespace Chirp;

use Chirp\IO\Console;
use Chirp\Storage\PersistenceManager;
use Chirp\CommandFactory\CommandDetector;

/**
 * Class ChirpApplication
 * Main Application for Chirp cli based social network.
 */
class ChirpApplication implements IApplication
{
    /**
     * @var Console
     */
    private $console;

    /**
     * @var PersistenceManager
     */
    private $persistenceManager;

    /**
     * @var CommandDetector
     */
    private $commandDetector;

    /**
     * ChirpApplication constructor.
     *
     * @param bool|resource $inputStream
     * @param bool|resource $outputStream
     */
    public function __construct($inputStream = STDIN, $outputStream = STDOUT)
    {
        $this->console = new Console($inputStream, $outputStream);
        $this->persistenceManager = new PersistenceManager();
        $this->commandDetector = new CommandDetector($this, $this->persistenceManager);
    }

    /**
     * Single application loop function.
     */
    private function applicationLoop()
    {
        $command = $this->commandDetector->makeCommand($this->console->getInput());
        $commandResponse = $command->execute();

        foreach ($commandResponse->messages() as $message) {
            $this->console->setOutput($message);
        }
    }

    /**
     * Current application running status.
     *
     * @var bool
     */
    private $running = false;

    /**
     * Start the application runtime.
     */
    public function run(): void
    {
        $this->running = true;

        while ($this->isRunning() and !TEST) {
            // @codeCoverageIgnoreStart
            $this->applicationLoop();
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * Start the application runtime.
     */
    public function start(): void
    {
        $this->running = true;

        $this->run();
    }

    /**
     * Force application termination.
     */
    public function terminate(): void
    {
        $this->running = false;
    }

    /**
     * Return the internal application running status.
     *
     * @return bool
     */
    public function isRunning(): bool
    {
        return $this->running;
    }
}
