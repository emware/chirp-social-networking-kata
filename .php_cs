<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->exclude('.vagrant')
    ->exclude('storage')
    ->notPath('src/Symfony/Component/Translation/Tests/fixtures/resources.php')
    ->in(__DIR__)
;

return PhpCsFixer\Config::create()
    ->setRules(array(
        '@PSR2' => true,
        '@Symfony' => true,
        // 'strict_param' => true,
        'single_quote' => true,
        'trim_array_spaces' => true,
        'trailing_comma_in_multiline_array' => true,
        'array_syntax' => array('syntax' => 'short'),
        'ordered_imports' => array('sortAlgorithm' => 'length'),
    ))
    ->setFinder($finder)
;