<?php

require __DIR__.'/vendor/autoload.php';

define('TEST', false);

use Chirp\ChirpApplication;

( new ChirpApplication() )->run();
