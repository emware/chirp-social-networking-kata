<?php

namespace Chirp\Test;

use ReflectionClass;
use Chirp\ChirpApplication;
use PHPUnit\Framework\TestCase;

class ChirpStandardAcceptanceTest extends TestCase
{
    /**
     * @var ChirpApplication
     */
    private static $application;

    /**
     * @var resource
     */
    private static $inputStream;

    /**
     * @var resource
     */
    private static $outputStream;

    public static function setUpBeforeClass()
    {
        self::$inputStream = fopen('php://memory', 'w+');
        self::$outputStream = fopen('php://memory', 'r+');
        self::$application = new ChirpApplication(self::$inputStream, self::$outputStream);
    }

    private function writeInputAndReturnOutput(string $input): string
    {
        ftruncate(self::$inputStream, 0);
        ftruncate(self::$outputStream, 0);
        fwrite(self::$inputStream, $input);
        rewind(self::$inputStream);

        $reflectionClass = new ReflectionClass(ChirpApplication::class);
        $applicationLoopMethod = $reflectionClass->getMethod('applicationLoop');
        $applicationLoopMethod->setAccessible(true);

        $applicationLoopMethod->invoke(self::$application);

        rewind(self::$outputStream);

        return trim(stream_get_contents(self::$outputStream), "\n ");
    }

    public function testCreateUsers()
    {
        $this->assertEquals('> User created successfully!', $this->writeInputAndReturnOutput('adduser john_doe'));
        $this->assertEquals('> User created successfully!', $this->writeInputAndReturnOutput('adduser mark_black'));
    }

    public function testUserCanChirp()
    {
        $this->assertEquals('', $this->writeInputAndReturnOutput('john_doe -> chirp test'));
        $this->assertEquals('', $this->writeInputAndReturnOutput('john_doe -> chirp test 2'));
        $this->assertEquals('', $this->writeInputAndReturnOutput('mark_black -> chirp test 3'));
    }

    public function testUserCanReadChirps()
    {
        $this->assertEquals("> chirp test 2 (1 second ago)\n> chirp test (1 second ago)", $this->writeInputAndReturnOutput('john_doe'));
    }

    public function testUserCanFollowAnotherUser()
    {
        $this->assertEquals('> john_doe now is following mark_black', $this->writeInputAndReturnOutput('john_doe follows mark_black'));
    }

    public function testUserCanVisualizeAWallWithAllChirpsFromFollowedUsers()
    {
        $this->assertEquals("> mark_black - chirp test 3 (1 second ago)\n> john_doe - chirp test 2 (1 second ago)\n> john_doe - chirp test (1 second ago)", $this->writeInputAndReturnOutput('john_doe wall'));
    }

    public function testApplicationCanBeTerminated()
    {
        $this->assertEquals('> Application terminated', $this->writeInputAndReturnOutput('exit'));
    }
}
