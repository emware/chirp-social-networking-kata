<?php

namespace Chirp\Test;

define('TEST', true);

use Chirp\IApplication;
use Chirp\ChirpApplication;
use PHPUnit\Framework\TestCase;

/**
 * Class ApplicationTest.
 *
 * @covers \Chirp\ChirpApplication
 */
class ChirpTest extends TestCase
{
    public function testIsInstanceOfIApplication()
    {
        $app = new ChirpApplication();

        $this->assertInstanceOf(IApplication::class, $app);
    }

    public function testApplicationContinueToRunUntilTerminated()
    {
        $app = new ChirpApplication();

        $app->start();

        $this->assertTrue($app->isRunning());

        $app->terminate();

        $this->assertFalse($app->isRunning());
    }
}
