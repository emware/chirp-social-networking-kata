<?php

namespace Chirp\Test\Storage;

use Chirp\Entity\User;
use Chirp\Entity\Chirp;
use Chirp\Test\ChirpTestCase;
use Chirp\Storage\PersistenceManager;

/**
 * Class PersistenceManagerTest.
 *
 * @covers \Chirp\Storage\PersistenceManager
 */
class PersistenceManagerTest extends ChirpTestCase
{
    public function testCanStoreAnEntity()
    {
        $persistenceManager = new PersistenceManager();

        $mockUser = $this->getUserMock();

        $this->assertTrue($persistenceManager->store($mockUser));

        $persistenceManager->select(get_class($mockUser));
        $this->assertEquals([$mockUser], $persistenceManager->get());
    }

    public function testFlushWillRemoveAllPersistedEntity()
    {
        $persistenceManager = new PersistenceManager();

        $mockUser = $this->getUserMock();

        $this->assertTrue($persistenceManager->store($mockUser));

        $persistenceManager->flush();

        $persistenceManager->select(get_class($mockUser));
        $this->assertEquals([], $persistenceManager->get());
    }

    /**
     * @return array
     */
    public function entitiesProvider()
    {
        $userSet = [];
        $ChirpSet = [];

        for ($i = 0; $i < 5; ++$i) {
            $userMock = $this->getMockBuilder(User::class)
                ->setMethods(null)
                ->setConstructorArgs([sprintf('username_%s', $i)])
                ->getMock();

            $ChirpMock = $this->getMockBuilder(Chirp::class)
                ->setConstructorArgs([$userMock, uniqid()])
                ->getMock();

            $ChirpSet[] = $ChirpMock;
            $userSet[] = $userMock;
        }

        return [
            [
                $userSet,
                $ChirpSet,
            ],
        ];
    }

    /**
     * @dataProvider entitiesProvider
     *
     * @param array $firstSet
     * @param array $secondSet
     */
    public function testCanStoreMultipleEntityForDifferentTypesOrderedByDateCreatedDesc($firstSet, $secondSet)
    {
        $persistenceManager = new PersistenceManager();

        foreach ($firstSet as $entity) {
            $this->assertTrue($persistenceManager->store($entity));
        }

        foreach ($secondSet as $entity) {
            $this->assertTrue($persistenceManager->store($entity));
        }

        $persistenceManager->select(get_class($firstSet[0]));
        $this->assertEquals(array_reverse($firstSet), $persistenceManager->get());

        $persistenceManager->select(get_class($secondSet[0]));
        $this->assertEquals(array_reverse($secondSet), $persistenceManager->get());
    }

    /**
     * @dataProvider entitiesProvider
     *
     * @param array $firstSet
     */
    public function testCannotStoreDuplicatedEntity($firstSet)
    {
        $persistenceManager = new PersistenceManager();

        foreach ($firstSet as $entity) {
            $this->assertTrue($persistenceManager->store($entity));
            $this->assertFalse($persistenceManager->store($entity));
        }
    }

    /**
     * @dataProvider entitiesProvider
     *
     * @param array $userDataSet
     */
    public function testCanFilterSelectedEntity($userDataSet)
    {
        $persistenceManager = new PersistenceManager();

        foreach ($userDataSet as $entity) {
            $persistenceManager->store($entity);
        }

        $filteredElement = array_shift($userDataSet);
        $term = $filteredElement->getUsername();

        $persistenceManager
            ->select(get_class($filteredElement))
            ->filter(function ($entity) use ($term) {
                if ($entity instanceof User and $entity->getUsername() == $term) {
                    return true;
                }

                return false;
            });

        $this->assertEquals([$filteredElement], $persistenceManager->get());
    }
}
