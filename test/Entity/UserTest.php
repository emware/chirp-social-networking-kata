<?php

namespace Chirp\Test\Entity;

use Chirp\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest.
 *
 * @covers \Chirp\Entity\User
 */
class UserTest extends TestCase
{
    public function testUserShouldBeCreatedWithAnUsername()
    {
        $username = uniqid();
        $user = new User($username);
        $this->assertEquals($username, $user->getUsername());
    }

    public function testUserCanFollowOtherUsers()
    {
        $mark = new User(uniqid());
        $ketty = new User(uniqid());
        $nick = new User(uniqid());

        $mark->follow($nick);
        $mark->follow($ketty);

        $this->assertArraySubset([$nick, $ketty], $mark->getFollowedUsers());
    }

    public function testUserFollowAnotherUserOnlyOneTime()
    {
        $mark = new User(uniqid());
        $ketty = new User(uniqid());

        $mark->follow($ketty);
        $mark->follow($ketty);

        $this->assertEquals([$ketty], $mark->getFollowedUsers());
    }
}
