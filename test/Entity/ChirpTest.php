<?php

namespace Chirp\Test\Entity;

use Carbon\Carbon;
use Chirp\Entity\Chirp;
use Chirp\Test\ChirpTestCase;

/**
 * Class ChirpTest.
 *
 * @covers \Chirp\Entity\Chirp
 */
class ChirpTest extends ChirpTestCase
{
    public function testHasMessageAndCreatorProperty()
    {
        $mockUser = $this->getUserMock();

        $content = uniqid();

        $Chirp = new Chirp($mockUser, $content);

        $this->assertEquals($content, $Chirp->message());
        $this->assertEquals($mockUser, $Chirp->createdBy());
    }

    public function testCreatedAtReturnAnInstanceOfCarbon()
    {
        $mockUser = $this->getUserMock();

        $content = uniqid();

        $Chirp = new Chirp($mockUser, $content);
        $this->assertInstanceOf(Carbon::class, $Chirp->createdAt());
    }
}
