<?php

namespace Chirp\Test\CommandFactory\DTO;

use PHPUnit\Framework\TestCase;
use Chirp\CommandFactory\DTO\ResponseDTO;

/**
 * Class ResponseDTOTest.
 *
 * @covers \Chirp\CommandFactory\DTO\ResponseDTO
 */
class ResponseDTOTest extends TestCase
{
    public function commandResponseData()
    {
        return [
            [true, ['Some random message']],
            [true, ['Some random message', 'Another random message']],
            [false, ['Some error message']],
            [false, ['Some error message', 'Another error message']],
        ];
    }

    /**
     * @dataProvider commandResponseData
     */
    public function testProvideCommandSuccessStatus($success, $messages)
    {
        $response = new ResponseDTO($success, $messages);

        $this->assertEquals($success, $response->success());
    }

    /**
     * @dataProvider commandResponseData
     */
    public function testCommandResponseReturnAnArrayOfOutputMessages($success, $messages)
    {
        $response = new ResponseDTO($success, $messages);

        $this->assertEquals($messages, $response->messages());
    }
}
