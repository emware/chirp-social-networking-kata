<?php

namespace Chirp\Test\CommandFactory\DTO;

use PHPUnit\Framework\TestCase;
use Chirp\CommandFactory\DTO\InputDTO;

/**
 * Class InputDTOTest.
 *
 * @covers \Chirp\CommandFactory\DTO\InputDTO
 */
class InputDTOTest extends TestCase
{
    public function testRequireOperatorValue()
    {
        $this->expectException('ArgumentCountError');
        new InputDTO();
    }

    public function testCanTransportUsernameAndParameterValues()
    {
        $operator = uniqid();

        $dto = new InputDTO($operator);

        $username = uniqid();
        $parameter = uniqid();

        $dto
            ->setUsername($username)
            ->setParameter($parameter);

        $this->assertEquals($username, $dto->getUsername());
        $this->assertEquals($parameter, $dto->getParameter());
        $this->assertEquals($operator, $dto->getOperator());
    }
}
