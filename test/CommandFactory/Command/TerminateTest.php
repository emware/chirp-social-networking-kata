<?php

namespace Chirp\Test\CommandFactory\Command;

use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\DTO\ResponseDTO;
use Chirp\CommandFactory\Command\Terminate;

/**
 * Class TerminateTest.
 *
 * @covers \Chirp\CommandFactory\Command\Terminate
 */
class TerminateTest extends ChirpTestCase
{
    public function testShouldBeInstantiatedWithApplicationInjection()
    {
        $this->expectException('Error');
        new Terminate();

        $applicationMock = $this->getApplicationMock();

        new Terminate($applicationMock);
    }

    public function testApplicationShouldStopRunningAfterCommandExecution()
    {
        $applicationMock = $this->getApplicationMock();

        $applicationMock->expects($this->once())->method('terminate');
        $applicationMock->method('isRunning')->willReturn(true);

        $terminateCommand = new Terminate($applicationMock);

        $responseDto = $terminateCommand->execute();

        $this->assertInstanceOf(ResponseDTO::class, $responseDto);
    }
}
