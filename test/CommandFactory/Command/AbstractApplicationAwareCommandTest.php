<?php

namespace Chirp\Test\CommandFactory\Command;

use ReflectionClass;
use Chirp\IApplication;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\AbstractCommand;
use Chirp\CommandFactory\Command\AbstractApplicationAwareCommand;

/**
 * Class AbstractApplicationAwareCommandTest.
 *
 * @covers \Chirp\CommandFactory\Command\AbstractApplicantUserAwareCommand
 */
class AbstractApplicationAwareCommandTest extends ChirpTestCase
{
    public function testIsSubclassOfAbstractCommand()
    {
        $abstractCommand = $this->getMockBuilder(AbstractApplicationAwareCommand::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertInstanceOf(AbstractCommand::class, $abstractCommand);
    }

    public function testShouldBeInstantiatedWithApplicationAndAParameter()
    {
        $applicationMock = $this->getApplicationMock();

        $abstractCommand = $this->getMockBuilder(AbstractApplicationAwareCommand::class)
            ->setConstructorArgs([$applicationMock, uniqid()])
            ->getMockForAbstractClass();

        $reflectionClass = new ReflectionClass(AbstractApplicationAwareCommand::class);
        $getArgumentsMethod = $reflectionClass->getMethod('getApplication');
        $getArgumentsMethod->setAccessible(true);

        $this->assertInstanceOf(IApplication::class, $getArgumentsMethod->invoke($abstractCommand));
    }
}
