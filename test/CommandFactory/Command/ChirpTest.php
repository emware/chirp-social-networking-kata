<?php

namespace Chirp\Test\CommandFactory\Command;

use Chirp\Entity\User;
use Chirp\Test\ChirpTestCase;
use Chirp\Entity\Chirp as ChirpEntity;
use Chirp\CommandFactory\Command\Chirp;

/**
 * Class ChirpTest.
 *
 * @covers \Chirp\CommandFactory\Command\Chirp
 */
class ChirpTest extends ChirpTestCase
{
    public function testAddANewChirpEntityAssociatedToApplicantUserToPersistenceManager()
    {
        $username = uniqid();

        $userMock = $this->getMockBuilder(User::class)
            ->setMethodsExcept(['getUsername'])
            ->setConstructorArgs([$username])
            ->getMock();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock
            ->expects($this->atLeastOnce())
            ->method('store')
            ->with($this->callback(function ($subject) {
                return $subject instanceof ChirpEntity;
            }))
            ->willReturn(true);

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->willReturn([$userMock]);

        $chirpCommand = new Chirp($persistenceManagerMock, uniqid());

        $response = $chirpCommand->execute();

        $this->assertTrue($response->success());
    }
}
