<?php

namespace Chirp\Test\CommandFactory\Command;

use ReflectionClass;
use Chirp\Entity\User;
use Chirp\Test\ChirpTestCase;
use Chirp\Storage\Interfaces\IPersistenceManager;
use Chirp\CommandFactory\Command\AbstractApplicantUserAwareCommand;
use Chirp\CommandFactory\Command\AbstractPersistenceLayerAwareCommand;

/**
 * Class AbstractApplicantUserAwareCommandTest.
 *
 * @covers \Chirp\CommandFactory\Command\AbstractApplicantUserAwareCommand
 */
class AbstractApplicantUserAwareCommandTest extends ChirpTestCase
{
    public function testIsSubclassOfAbstractPersistenceLayerAwareCommand()
    {
        $abstractCommand = $this->getMockBuilder(AbstractApplicantUserAwareCommand::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertInstanceOf(AbstractPersistenceLayerAwareCommand::class, $abstractCommand);
    }

    public function testMustBeInstantiatedWithTheUsernameOfCurrentLoggedUser()
    {
        $this->expectException('TypeError');
        $this->getMockBuilder(AbstractApplicantUserAwareCommand::class)
            ->setConstructorArgs([uniqid()])
            ->getMockForAbstractClass();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $this->getMockBuilder(AbstractPersistenceLayerAwareCommand::class)
            ->setConstructorArgs([$persistenceManagerMock, uniqid(), uniqid()])
            ->getMockForAbstractClass();
    }

    public function systemProvider()
    {
        $username = uniqid();

        $userMock = $this->getMockBuilder(User::class)
                         ->setMethodsExcept(['getUsername'])
                         ->setConstructorArgs([$username])
                         ->getMock();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->expects($this->atLeastOnce())->method('get')->willReturn([$userMock]);

        return [
            [$username, $userMock, $persistenceManagerMock],
        ];
    }

    /**
     * @dataProvider systemProvider
     *
     * @param string              $username
     * @param User                $userMock
     * @param IPersistenceManager $persistenceManagerMock
     */
    public function testCanFindUserByUsername(string $username, User $userMock, IPersistenceManager $persistenceManagerMock)
    {
        $abstractCommand = $this->getMockBuilder(AbstractApplicantUserAwareCommand::class)
            ->setConstructorArgs([$persistenceManagerMock, $username, uniqid()])
            ->getMockForAbstractClass();

        $reflectionClass = new ReflectionClass(AbstractApplicantUserAwareCommand::class);
        $getPersistenceManagerMethod = $reflectionClass->getMethod('findUserByUsername');
        $getPersistenceManagerMethod->setAccessible(true);

        $this->assertEquals($userMock, $getPersistenceManagerMethod->invokeArgs($abstractCommand, [$username]));
    }

    /**
     * @dataProvider systemProvider
     *
     * @param string              $username
     * @param User                $userMock
     * @param IPersistenceManager $persistenceManagerMock
     */
    public function testAutomaticallyFindTheApplicantUserFromUsernameProvidedAtConstructionTime(string $username, User $userMock, IPersistenceManager $persistenceManagerMock)
    {
        $abstractCommand = $this->getMockBuilder(AbstractApplicantUserAwareCommand::class)
                                ->setConstructorArgs([$persistenceManagerMock, $username, uniqid()])
                                ->getMockForAbstractClass();

        $reflectionClass = new ReflectionClass(AbstractApplicantUserAwareCommand::class);
        $getPersistenceManagerMethod = $reflectionClass->getMethod('getApplicantUser');
        $getPersistenceManagerMethod->setAccessible(true);

        $this->assertEquals($userMock, $getPersistenceManagerMethod->invokeArgs($abstractCommand, [$username]));
    }
}
