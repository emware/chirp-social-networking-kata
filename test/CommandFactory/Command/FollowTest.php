<?php

namespace Chirp\Test\CommandFactory\Command;

use Chirp\Entity\User;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\Follow;
use Chirp\Storage\Interfaces\IPersistenceManager;

/**
 * Class FollowTest.
 *
 * @covers \Chirp\CommandFactory\Command\Follow
 */
class FollowTest extends ChirpTestCase
{
    public function userProvider()
    {
        $user = $this->getMockBuilder(User::class)
                     ->setMethods(null)
                     ->setConstructorArgs([uniqid()])
                     ->getMock();

        $followedUser = $this->getMockBuilder(User::class)
                             ->setMethods(null)
                             ->setConstructorArgs([uniqid()])
                             ->getMock();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->willReturnOnConsecutiveCalls([$user], [$followedUser], [$user], [$followedUser]);

        return [[$user, $followedUser, $persistenceManagerMock]];
    }

    /**
     * @dataProvider userProvider
     *
     * @param \PHPUnit_Framework_MockObject_MockObject $user
     * @param \PHPUnit_Framework_MockObject_MockObject $followedUser
     * @param IPersistenceManager                      $persistenceManagerMock
     */
    public function testWillMakeCurrentUserFollowsAnotherUser(\PHPUnit_Framework_MockObject_MockObject $user, \PHPUnit_Framework_MockObject_MockObject $followedUser, IPersistenceManager $persistenceManagerMock)
    {
        $response = (new Follow($persistenceManagerMock, $user->getUsername(), $followedUser->getUsername()))->execute();

        $this->assertTrue($response->success());
    }

    /**
     * @dataProvider userProvider
     *
     * @param \PHPUnit_Framework_MockObject_MockObject $user
     * @param \PHPUnit_Framework_MockObject_MockObject $followedUser
     * @param IPersistenceManager                      $persistenceManagerMock
     */
    public function testUserCanFollowAnotherUserOnlyOneTime(\PHPUnit_Framework_MockObject_MockObject $user, \PHPUnit_Framework_MockObject_MockObject $followedUser, IPersistenceManager $persistenceManagerMock)
    {
        (new Follow($persistenceManagerMock, $user->getUsername(), $followedUser->getUsername()))->execute();
        $response = (new Follow($persistenceManagerMock, $user->getUsername(), $followedUser->getUsername()))->execute();

        $this->assertFalse($response->success());
    }

    public function testUserCannotFollowHimself()
    {
        $userMock = $this->getMockBuilder(User::class)
                     ->setMethodsExcept(['getUsername'])
                     ->setConstructorArgs([uniqid()])
                     ->getMock();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->willReturn([$userMock]);

        $response = (new Follow($persistenceManagerMock, $userMock->getUsername(), $userMock->getUsername()))->execute();

        $this->assertFalse($response->success());
    }
}
