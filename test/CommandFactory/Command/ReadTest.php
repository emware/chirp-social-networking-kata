<?php

namespace Chirp\CommandFactory\Command;

use Carbon\Carbon;
use Chirp\Entity\User;
use Chirp\Test\ChirpTestCase;
use Chirp\Entity\Chirp as ChirpEntity;

/**
 * Class ReadTest.
 *
 * @covers \Chirp\CommandFactory\Command\Read
 */
class ReadTest extends ChirpTestCase
{
    public function testWillReturnAllPostForTheApplicantUser()
    {
        $username = uniqid();

        $userMock = $this->getMockBuilder(User::class)
            ->setConstructorArgs([$username])
            ->getMock();

        $carbonMock = $this->getMockBuilder(Carbon::class)
            ->disableOriginalConstructor()
            ->getMock();

        $carbonMock->method('diffForHumans')->willReturn('1 second ago');

        $message1 = uniqid();
        $chirpMock1 = $this->getMockBuilder(ChirpEntity::class)
            ->setMethodsExcept(['message'])
            ->setConstructorArgs([$userMock, $message1])
            ->getMock();

        $chirpMock1->method('createdAt')->willReturn($carbonMock);

        $message2 = uniqid();
        $chirpMock2 = $this->getMockBuilder(ChirpEntity::class)
            ->setMethodsExcept(['message'])
            ->setConstructorArgs([$userMock, $message2])
            ->getMock();

        $chirpMock2->method('createdAt')->willReturn($carbonMock);

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->expects($this->atLeastOnce())->method('get')->willReturnOnConsecutiveCalls([$userMock], [$chirpMock1, $chirpMock2]);

        $response = (new Read($persistenceManagerMock, $username))->execute();

        $message1 .= ' (1 second ago)';
        $message2 .= ' (1 second ago)';
        $this->assertTrue($response->success());
        $this->assertEquals([$message1, $message2], $response->messages());
    }
}
