<?php

namespace Chirp\Test\CommandFactory\Command;

use Carbon\Carbon;
use Chirp\Entity\User;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\Wall;
use Chirp\Entity\Chirp as ChirpEntity;

/**
 * Class WallTest.
 *
 * @covers \Chirp\CommandFactory\Command\Wall
 */
class WallTest extends ChirpTestCase
{
    public function testWillDisplayChirpsFromApplicantAndAllItsFollowedUsersOrderedByDateCreationDesc()
    {
        $username = uniqid();

        $applicantMock = $this->getMockBuilder(User::class)
            ->setConstructorArgs([$username])
            ->getMock();

        $userMock = $this->getMockBuilder(User::class)
            ->setConstructorArgs([uniqid()])
            ->getMock();

        $carbonMock = $this->getMockBuilder(Carbon::class)
                           ->disableOriginalConstructor()
                           ->getMock();

        $carbonMock->method('diffForHumans')->willReturn('1 second ago');

        $message1 = uniqid();
        $chirpMock1 = $this
            ->getMockBuilder(ChirpEntity::class)
            ->setMethodsExcept(['message'])
            ->setConstructorArgs([$applicantMock, $message1])
            ->getMock();

        $chirpMock1->method('createdAt')->willReturn($carbonMock);
        $chirpMock1->method('createdBy')->willReturn($applicantMock);

        $message2 = uniqid();
        $chirpMock2 = $this
            ->getMockBuilder(ChirpEntity::class)
            ->setMethodsExcept(['message'])
            ->setConstructorArgs([$userMock, $message2])
            ->getMock();

        $chirpMock2->method('createdAt')->willReturn($carbonMock);
        $chirpMock2->method('createdBy')->willReturn($userMock);

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->expects($this->atLeastOnce())->method('get')->willReturnOnConsecutiveCalls([$userMock], [$chirpMock1, $chirpMock2]);

        $response = (new Wall($persistenceManagerMock, $username))->execute();

        $message1 = sprintf('%s - %s (1 second ago)', $applicantMock->getUsername(), $chirpMock1->message());
        $message2 = sprintf('%s - %s (1 second ago)', $userMock->getUsername(), $chirpMock2->message());

        $this->assertTrue($response->success());
        $this->assertEquals([$message1, $message2], $response->messages());
    }
}
