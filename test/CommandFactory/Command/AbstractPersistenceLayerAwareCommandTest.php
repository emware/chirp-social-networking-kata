<?php

namespace Chirp\Test\CommandFactory\Command;

use ReflectionClass;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\AbstractCommand;
use Chirp\CommandFactory\Command\AbstractPersistenceLayerAwareCommand;

/**
 * Class AbstractPersistenceLayerAwareCommandTest.
 *
 * @covers \Chirp\CommandFactory\Command\AbstractPersistenceLayerAwareCommand
 */
class AbstractPersistenceLayerAwareCommandTest extends ChirpTestCase
{
    public function testIsSubclassOfAbstractCommand()
    {
        $abstractCommand = $this->getMockBuilder(AbstractPersistenceLayerAwareCommand::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertInstanceOf(AbstractCommand::class, $abstractCommand);
    }

    public function testMustBeInstantiatedWithAPersistenceManagerAndAParameter()
    {
        $this->expectException('TypeError');
        $this->getMockBuilder(AbstractPersistenceLayerAwareCommand::class)
            ->setConstructorArgs([uniqid()])
            ->getMockForAbstractClass();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $this->getMockBuilder(AbstractPersistenceLayerAwareCommand::class)
            ->setConstructorArgs([$persistenceManagerMock, uniqid()])
            ->getMockForAbstractClass();
    }

    public function testShouldReturnPersistenceManagerFromProtectedMethod()
    {
        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $abstractCommand = $this->getMockBuilder(AbstractPersistenceLayerAwareCommand::class)
             ->setConstructorArgs([$persistenceManagerMock, uniqid()])
             ->getMockForAbstractClass();

        $reflectionClass = new ReflectionClass(AbstractPersistenceLayerAwareCommand::class);
        $getPersistenceManagerMethod = $reflectionClass->getMethod('getPersistenceManager');
        $getPersistenceManagerMethod->setAccessible(true);

        $this->assertEquals($persistenceManagerMock, $getPersistenceManagerMethod->invoke($abstractCommand));
    }
}
