<?php

namespace Chirp\Test\CommandFactory\Command;

use Chirp\Entity\User;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\Adduser;
use Chirp\Storage\Interfaces\IPersistenceManager;

/**
 * Class AdduserTest.
 *
 * @covers \Chirp\CommandFactory\Command\Adduser
 */
class AdduserTest extends ChirpTestCase
{
    public function testCanAddANewUser()
    {
        $username = uniqid();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock
            ->expects($this->once())
            ->method('store')
            ->with($this->isInstanceOf(User::class))
            ->will($this->returnCallback(function ($subject) use ($username) {
                return (bool) (($subject instanceof User) and ($subject->getUsername() == $username));
            }));

        $response = (new Adduser($persistenceManagerMock, $username))->execute();

        $this->assertTrue($response->success());
    }

    public function testUserCouldNotHaveDuplicatedUsername()
    {
        $username = uniqid();

        $persistenceManagerMock = $this->getMockBuilder(IPersistenceManager::class)
                                       ->disableOriginalConstructor()
                                       ->getMock();

        $persistenceManagerMock
            ->expects($this->atLeastOnce())
            ->method('store')
            ->with($this->isInstanceOf(User::class))
            ->will($this->returnCallback(function ($subject) use ($username) {
                return (bool) (($subject instanceof User) and ($subject->getUsername() == $username));
            }));

        $response = (new Adduser($persistenceManagerMock, $username))->execute();

        $this->assertTrue($response->success());

        $userMock = $this->getMockBuilder(User::class)
            ->setConstructorArgs([$username])
            ->getMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->willReturn([$userMock]);

        $response = (new Adduser($persistenceManagerMock, $username))->execute();

        $this->assertFalse($response->success());
    }

    public function testCannotAddUserWithoutUsername()
    {
        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $response = (new Adduser($persistenceManagerMock, ''))->execute();

        $this->assertFalse($response->success());
    }
}
