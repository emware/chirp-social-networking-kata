<?php

namespace Chirp\Test\CommandFactory\Command;

use ReflectionClass;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\AbstractCommand;
use Chirp\CommandFactory\Interfaces\IGenericCommand;

/**
 * Class AbstractCommandTest.
 *
 * @covers \Chirp\CommandFactory\Command\AbstractCommand
 */
class AbstractCommandTest extends ChirpTestCase
{
    public function testAbstractCommandImplementsGenericCommandInterface()
    {
        $abstractCommand = $this->getMockBuilder(AbstractCommand::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->assertInstanceOf(IGenericCommand::class, $abstractCommand);
    }

    public function getCommandArguments()
    {
        return [
            ['test string'],
            [new \DateTime()],
            [
                ['test', 'array', 'argument'],
            ],
        ];
    }

    /**
     * @dataProvider getCommandArguments
     */
    public function testAcceptParameterInjectionAtConstructionTime($argument)
    {
        $abstractCommand = $this->getMockBuilder(AbstractCommand::class)
            ->setConstructorArgs([$argument])
            ->getMockForAbstractClass();

        $reflectionClass = new ReflectionClass(AbstractCommand::class);
        $getArgumentsMethod = $reflectionClass->getMethod('getParameter');
        $getArgumentsMethod->setAccessible(true);

        $this->assertEquals($argument, $getArgumentsMethod->invoke($abstractCommand));
    }
}
