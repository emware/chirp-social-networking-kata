<?php

namespace Chirp\Test\CommandFactory;

use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\CommandDetector;
use Chirp\CommandFactory\Command\AbstractCommand;

/**
 * Class CommandDetectorTest.
 *
 * @covers \Chirp\CommandFactory\CommandDetector
 */
class CommandDetectorTest extends ChirpTestCase
{
    public function testShouldBeInstantiatedWithAPersistenceManagerAndApplication()
    {
        $this->expectException('Error');
        new CommandDetector();

        $persistenceManagerMock = $this->getPersistenceManagerMock();
        $applicationMock = $this->getApplicationMock();

        new CommandDetector($applicationMock, $persistenceManagerMock);
    }

    /**
     * @dataProvider commandInputDataProvider
     */
    public function testMakeExpectedCommandsFromStatements($class, $statement)
    {
        $userMock = $this->getUserMock();
        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->will($this->returnCallback(function () use ($class, $userMock) {
            if (is_subclass_of($class, AbstractCommand::class, true)) {
                return [$userMock];
            }
        }));

        $detector = new CommandDetector($this->getApplicationMock(), $persistenceManagerMock);

        $this->assertInstanceOf($class, $detector->makeCommand($statement));
    }

    public function testWillThrowExceptionWithEmptyStatement()
    {
        $detector = new CommandDetector($this->getApplicationMock(), $this->getPersistenceManagerMock());

        $this->expectException('Exception');
        $detector->makeCommand('');
    }
}
