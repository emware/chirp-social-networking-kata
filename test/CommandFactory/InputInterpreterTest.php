<?php

namespace Chirp\Test\CommandFactory;

use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\InputInterpreter;

/**
 * Class InputInterpreterTest.
 *
 * @covers \Chirp\CommandFactory\InputInterpreter
 */
class InputInterpreterTest extends ChirpTestCase
{
    public function testInterpretSingleWordStatement()
    {
        $statementParser = new InputInterpreter([], []);

        $statement = uniqid();
        $dto = $statementParser->interpret($statement);

        $this->assertInstanceOf(InputDTO::class, $dto);

        $this->assertEmpty($dto->getOperator());
    }

    public function randomStatements()
    {
        return [
            [
                'john_doe follows mark_low',
                ['operator' => 'follows', 'username' => 'john_doe', 'parameter' => 'mark_low'],
            ],
            [
                'john_doe -> today is a beautiful sunny day',
                ['operator' => '->', 'username' => 'john_doe', 'parameter' => 'today is a beautiful sunny day'],
            ],
            [
                'john_doe wall',
                ['operator' => 'wall', 'username' => 'john_doe', 'parameter' => ''],
            ],
            [
                'john_doe',
                ['operator' => '', 'username' => 'john_doe', 'parameter' => ''],
            ],
            [
                'exit',
                ['operator' => 'exit', 'username' => '', 'parameter' => ''],
            ],
            [
                'adduser test_user',
                ['operator' => 'adduser', 'username' => '', 'parameter' => 'test_user'],
            ],
        ];
    }

    /**
     * @dataProvider randomStatements
     */
    public function testInterpretCanAnalyzeInputAndMatchCommandSyntaxRules($statement, $results)
    {
        $operatorList = ['wall', 'follows', '->'];
        $systemOperatorList = ['exit', 'adduser'];

        $statementParser = new InputInterpreter($operatorList, $systemOperatorList);

        $dto = $statementParser->interpret($statement);

        $this->assertEquals($results['operator'], $dto->getOperator());
        $this->assertEquals($results['username'], $dto->getUsername());
        $this->assertEquals($results['parameter'], $dto->getParameter());
    }
}
