<?php

namespace Chirp\Test\CommandFactory\Factory;

use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\Command\Read;
use Chirp\CommandFactory\Command\Wall;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Command\Chirp;
use Chirp\CommandFactory\Command\Follow;
use Chirp\Storage\Interfaces\IPersistenceManager;
use Chirp\CommandFactory\Factory\AbstractCommandFactory;
use Chirp\CommandFactory\Factory\ApplicantUserAwareCommandFactory;

/**
 * Class ApplicantUserAwareCommandFactoryTest.
 */
class ApplicantUserAwareCommandFactoryTest extends ChirpTestCase
{
    public function testIsSubclassOfAbstractCommandFactory()
    {
        $persistenceLayerMock = $this->getPersistenceManagerMock();

        $applicationMock = $this->getApplicationMock();

        $factory = new ApplicantUserAwareCommandFactory($applicationMock, $persistenceLayerMock);

        $this->assertInstanceOf(AbstractCommandFactory::class, $factory);
    }

    public function validCommandInputs()
    {
        return [
            [Follow::class, ['operator' => 'follows', 'username' => uniqid(), 'parameter' => uniqid()]],
            [Wall::class, ['operator' => 'wall', 'username' => uniqid(), 'parameter' => uniqid()]],
            [Read::class, ['operator' => '', 'username' => uniqid(), 'parameter' => uniqid()]],
            [Chirp::class, ['operator' => '->', 'username' => uniqid(), 'parameter' => uniqid()]],
        ];
    }

    /**
     * @dataProvider validCommandInputs
     *
     * @param $class
     * @param $parameters
     */
    public function testMakeExpectedCommandsFromStatements($class, $parameters)
    {
        $userMock = $this->getUserMock();

        $persistenceManagerMock = $this->getMockBuilder(IPersistenceManager::class)
                                       ->disableOriginalConstructor()
                                       ->getMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->willReturn([$userMock]);

        $applicationMock = $this->getApplicationMock();

        $dtoMock = $this->getMockBuilder(InputDTO::class)
                        ->disableOriginalConstructor()
                        ->getMock();

        $dtoMock->method('getOperator')->willReturn($parameters['operator']);
        $dtoMock->method('getParameter')->willReturn($parameters['parameter']);
        $dtoMock->method('getUsername')->willReturn($parameters['username']);

        $factory = new ApplicantUserAwareCommandFactory($applicationMock, $persistenceManagerMock);

        $this->assertInstanceOf($class, $factory->makeCommand($dtoMock));
    }
}
