<?php

namespace Chirp\Test\CommandFactory\Factory;

use PHPUnit\Framework\TestCase;
use Chirp\CommandFactory\Interfaces\ICommandFactory;
use Chirp\CommandFactory\Factory\AbstractCommandFactory;

/**
 * Class AbstractCommandFactoryTest.
 */
class AbstractCommandFactoryTest extends TestCase
{
    public function testImplementsICommandFactory()
    {
        $abstractFactory = $this->getMockBuilder(AbstractCommandFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertInstanceOf(ICommandFactory::class, $abstractFactory);
    }
}
