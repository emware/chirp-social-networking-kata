<?php

namespace Chirp\Test\CommandFactory\Factory;

use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Command\Terminate;
use Chirp\CommandFactory\Factory\AbstractCommandFactory;
use Chirp\CommandFactory\Factory\ApplicationAwareCommandFactory;

/**
 * Class ApplicationAwareCommandFactoryTest.
 */
class ApplicationAwareCommandFactoryTest extends ChirpTestCase
{
    public function testIsSubclassOfAbstractCommandFactory()
    {
        $persistenceLayerMock = $this->getPersistenceManagerMock();

        $applicationMock = $this->getApplicationMock();

        $factory = new ApplicationAwarecommandFactory($applicationMock, $persistenceLayerMock);

        $this->assertInstanceOf(AbstractCommandFactory::class, $factory);
    }

    public function validCommandInputs()
    {
        return [
            [Terminate::class, ['operator' => 'exit', 'username' => '', 'parameter' => '']],
        ];
    }

    /**
     * @dataProvider validCommandInputs
     */
    public function testMakeExpectedCommandsFromStatements($class, $parameters)
    {
        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $applicationMock = $this->getApplicationMock();

        $dtoMock = $this->getMockBuilder(InputDTO::class)
            ->disableOriginalConstructor()
            ->getMock();

        $dtoMock->method('getOperator')->willReturn($parameters['operator']);
        $dtoMock->method('getParameter')->willReturn($parameters['parameter']);
        $dtoMock->method('getUsername')->willReturn($parameters['username']);

        $factory = new ApplicationAwareCommandFactory($applicationMock, $persistenceManagerMock);

        $this->assertInstanceOf($class, $factory->makeCommand($dtoMock));
    }
}
