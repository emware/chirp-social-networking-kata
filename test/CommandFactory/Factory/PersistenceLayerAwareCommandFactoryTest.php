<?php

namespace Chirp\Test\CommandFactory\Factory;

use Chirp\IApplication;
use Chirp\Test\ChirpTestCase;
use Chirp\CommandFactory\DTO\InputDTO;
use Chirp\CommandFactory\Command\Adduser;
use Chirp\CommandFactory\Factory\AbstractCommandFactory;
use Chirp\CommandFactory\Factory\PersistenceLayerAwareCommandFactory;

/**
 * Class PersistenceLayerAwareCommandFactoryTest.
 */
class PersistenceLayerAwareCommandFactoryTest extends ChirpTestCase
{
    public function testIsSubclassOfAbstractCommandFactory()
    {
        $persistenceLayerMock = $this->getPersistenceManagerMock();

        $applicationMock = $this->getMockBuilder(IApplication::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory = new PersistenceLayerAwareCommandFactory($applicationMock, $persistenceLayerMock);

        $this->assertInstanceOf(AbstractCommandFactory::class, $factory);
    }

    public function validCommandInputs()
    {
        return [
            [Adduser::class, ['operator' => 'adduser', 'username' => '', 'parameter' => uniqid()]],
        ];
    }

    /**
     * @dataProvider validCommandInputs
     */
    public function testMakeExpectedCommandsFromStatements($class, $parameters)
    {
        $userMock = $this->getUserMock();

        $persistenceManagerMock = $this->getPersistenceManagerMock();

        $persistenceManagerMock->method('select')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('filter')->willReturn($persistenceManagerMock);
        $persistenceManagerMock->method('get')->willReturn([$userMock]);

        $applicationMock = $this->getApplicationMock();

        $dtoMock = $this->getMockBuilder(InputDTO::class)
            ->disableOriginalConstructor()
            ->getMock();

        $dtoMock->method('getOperator')->willReturn($parameters['operator']);
        $dtoMock->method('getParameter')->willReturn($parameters['parameter']);
        $dtoMock->method('getUsername')->willReturn($parameters['username']);

        $factory = new PersistenceLayerAwareCommandFactory($applicationMock, $persistenceManagerMock);

        $this->assertInstanceOf($class, $factory->makeCommand($dtoMock));
    }
}
