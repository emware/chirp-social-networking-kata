<?php

namespace Chirp\Test;

use Chirp\Entity\User;
use Chirp\IApplication;
use PHPUnit\Framework\TestCase;
use Chirp\CommandFactory\Command\Read;
use Chirp\CommandFactory\Command\Wall;
use Chirp\CommandFactory\Command\Chirp;
use Chirp\CommandFactory\Command\Follow;
use Chirp\CommandFactory\Command\Adduser;
use Chirp\CommandFactory\Command\Terminate;
use Chirp\Storage\Interfaces\IPersistenceManager;

class ChirpTestCase extends TestCase
{
    /**
     * @return array
     */
    public function commandInputDataProvider()
    {
        return [
            [Follow::class, 'john_doe follows mark_low'],
            [Chirp::class, 'john_doe -> today is a beautiful sunny day'],
            [Read::class, 'john_doe'],
            [Wall::class, 'john_doe wall'],
            [Terminate::class, 'exit'],
            [Adduser::class, 'adduser new_user'],
        ];
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getPersistenceManagerMock()
    {
        return $this->getMockBuilder(IPersistenceManager::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getApplicationMock()
    {
        return $this->getMockBuilder(IApplication::class)
             ->disableOriginalConstructor()
             ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getUserMock()
    {
        return $this->getMockBuilder(User::class)
             ->disableOriginalConstructor()
             ->getMock();
    }
}
