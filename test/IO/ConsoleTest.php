<?php

namespace Chirp\Test\IO;

use Chirp\IO\Console;
use PHPUnit\Framework\TestCase;

/**
 * Class ConsoleTest.
 *
 * @covers \Chirp\IO\Console
 */
class ConsoleTest extends TestCase
{
    /**
     * @var Console
     */
    private $Console;

    /**
     * @var resource
     */
    private $inputStream;

    /**
     * @var resource
     */
    private $outputStream;

    protected function setUp()
    {
        parent::setUp();
        $this->inputStream = fopen('php://memory', 'w+');
        $this->outputStream = fopen('php://memory', 'r+');
        $this->Console = new Console($this->inputStream, $this->outputStream);
    }

    public function testConsoleReadSingleLineOfTextAtTime()
    {
        $payloads = [uniqid(), uniqid(), uniqid()];

        $shellInput = sprintf('%s%s', implode(PHP_EOL, $payloads), PHP_EOL);

        fwrite($this->inputStream, $shellInput);
        rewind($this->inputStream);

        foreach ($payloads as $payload) {
            $this->assertEquals($payload, $this->Console->getInput());
        }
    }

    public function testConsoleDoesNotTrimWhitespacesFromInput()
    {
        $payload = sprintf(' %s %s ', uniqid(), uniqid());

        fwrite($this->inputStream, $payload);
        rewind($this->inputStream);

        $this->assertEquals($payload, $this->Console->getInput());
    }

    public function testConsoleWriteOutputLines()
    {
        $payloads = [uniqid(), uniqid(), uniqid()];

        $expectedOutput = '';
        foreach ($payloads as $payload) {
            $this->Console->setOutput($payload);
            $expectedOutput .= sprintf('> %s%s', $payload, PHP_EOL);
        }

        rewind($this->outputStream);

        $streamOutput = stream_get_contents($this->outputStream);

        $this->assertEquals($expectedOutput, $streamOutput);
    }

    public function testConsoleWillNotWriteLineForEmptyOutput()
    {
        $payload = '';

        $this->Console->setOutput($payload);

        rewind($this->outputStream);

        $streamOutput = stream_get_contents($this->outputStream);

        $this->assertEquals($payload, $streamOutput);
    }
}
